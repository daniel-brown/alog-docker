Make www path to use, can symlink the alog git repo: ln -s <alog repo path> ./www

chmod 755 ./www

Start services: docker compose up -d

To start the servers. Everything should be available on localhost:8000

Based on https://thriveread.com/apache-php-with-docker-mysql-and-phpmyadmin/



See current log for all services: sudo docoker compose logs -f
Connect to service: sudo docker compose exec mysql-db bash

Rebuild after changes: sudo docker compose up --build -d
