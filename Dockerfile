# Use an official PHP runtime
FROM php:7.4-apache
# Enable Apache modules
RUN a2enmod rewrite
RUN echo "postfix postfix/mailname string services.adelaide.edu.au" | debconf-set-selections
RUN echo "postfix postfix/main_mailer_type string 'Internet Site'" | debconf-set-selections
RUN apt-get update -y && apt-get install -y libpng-dev libjpeg-dev zlib1g-dev libjpeg62-turbo-dev libfreetype6-dev postfix systemd
RUN docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/
# Install any extensions you need
RUN docker-php-ext-install mysqli pdo pdo_mysql gd
RUN docker-php-ext-enable gd

WORKDIR /var/www/html
# Copy the source code in /www into the container at /var/www/html
# COPY ../www .
ADD ./php.ini /usr/local/etc/php/conf.d/custom-php.ini
ADD ./main.cf /etc/postfix/main.cf
ADD ./launch.sh /launch.sh
RUN chmod +x /launch.sh
CMD ["/launch.sh"]
run chmod +r /etc/postfix/main.cf
